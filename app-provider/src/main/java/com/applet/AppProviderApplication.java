package com.applet;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.applet.dao")
@DubboComponentScan(basePackages = "com.applet.service")
public class AppProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppProviderApplication.class, args);
    }
}
