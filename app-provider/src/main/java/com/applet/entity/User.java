package com.applet.entity;

import com.applet.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 21:07
 * @version:1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private Integer id;

    private String name;

    private Long balance;

    private Date upTime;

    public static User DtotransitionEntity(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setBalance(userDTO.getBalance());
        user.setUpTime(new Date());
        return user;
    }
}
