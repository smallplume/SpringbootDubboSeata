package com.applet.dao;

import com.applet.entity.User;
import org.apache.ibatis.annotations.Insert;

/**
 * @author:359428217@qq.com
 * @date:2020/2/2 , 20:36
 * @version:1.0
 **/
public interface UserMapper {

    void insert(User user);
}
