package com.applet.service;

import com.applet.dao.UserMapper;
import com.applet.dto.UserDTO;
import com.applet.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 0:29
 * @version:1.0
 **/
@Slf4j
@Service(version = "1.0.0", interfaceClass = UserFacade.class)
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserMapper userDao;

    @Override
    public UserDTO inc(UserDTO userDTO) {
        User user = User.DtotransitionEntity(userDTO);
        userDao.insert(user);
        userDTO.setId(user.getId());
        return userDTO;
    }
}
