package com.applet.service;

import com.applet.dao.CompanyMapper;
import com.applet.dto.CompanyDTO;
import com.applet.entity.Company;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author:359428217@qq.com
 * @date:2020/2/2 , 0:05
 * @version:1.0
 **/
@Slf4j
@Service(version = "1.0.0", interfaceClass = CompanyFacade.class)
public class CompanyFacadeImpl implements CompanyFacade {

    @Autowired
    private CompanyMapper companyRepository;

    @Override
    public void inc(CompanyDTO companyDTO) {
        Company company = Company.DTOtransitionEntity(companyDTO);
        companyRepository.insert(company);
        log.info(">>>>>>>>>>>>>>>>>>{}", company.getId());
    }
}
