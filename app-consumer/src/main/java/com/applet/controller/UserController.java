package com.applet.controller;

import com.applet.common.BaseVo;
import com.applet.dto.CompanyDTO;
import com.applet.dto.UserDTO;
import com.applet.service.CompanyFacade;
import com.applet.service.UserFacade;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 0:41
 * @version:1.0
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Reference(version = "1.0.0")
    private UserFacade userFacade;

    @Reference(version = "1.0.0")
    private CompanyFacade companyFacade;

    @GlobalTransactional
    @PostMapping("/insert")
    public BaseVo insert(@RequestBody UserDTO userDTO) {
        UserDTO user = userFacade.inc(userDTO);
        int i = 10/0;
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setName(user.getCompanyName());
        companyDTO.setLegalPerson(user.getId());
        companyFacade.inc(companyDTO);
        return new BaseVo();
    }

}
