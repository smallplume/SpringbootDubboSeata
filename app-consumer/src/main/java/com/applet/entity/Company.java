package com.applet.entity;

import com.applet.dto.CompanyDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 23:56
 * @version:1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company implements Serializable {

    private Integer id;

    private String name;

    private Integer legalPerson;

    public static Company DTOtransitionEntity(CompanyDTO companyDTO) {
        Company company = new Company();
        company.setName(companyDTO.getName());
        company.setLegalPerson(companyDTO.getLegalPerson());
        return company;
    }

}
