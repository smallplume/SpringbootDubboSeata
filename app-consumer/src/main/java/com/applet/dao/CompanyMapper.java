package com.applet.dao;

import com.applet.dto.CompanyDTO;
import com.applet.entity.Company;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author:359428217@qq.com
 * @date:2020/2/2 , 19:22
 * @version:1.0
 **/
public interface CompanyMapper {

    void insert(Company company);

    @Select("Select id, name, legal_person from company where id = #{id}")
    CompanyDTO queryCompanyById(@Param(value = "id") Integer id);
}
