package com.applet.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author:359428217@qq.com
 * @date:2020/2/2 , 0:01
 * @version:1.0
 **/
@Data
public class CompanyDTO implements Serializable {

    private Integer id;

    private String name;

    private Integer legalPerson;
}
