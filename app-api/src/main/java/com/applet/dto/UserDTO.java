package com.applet.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 0:16
 * @version:1.0
 **/
@Data
public class UserDTO implements Serializable {
    /**
     * ID
     */
    private Integer id;
    /**
     * 用户名
     */
    private String name;
    /**
     * 余额
     */
    private Long balance;
    /**
     * 更改时间
     */
    private Date upTime;
    /**
     * 公司名称
     */
    private String companyName;
}
