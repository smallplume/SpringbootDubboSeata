package com.applet.service;

import com.applet.dto.UserDTO;

/**
 * @author:359428217@qq.com
 * @date:2020/2/1 , 0:22
 * @version:1.0
 **/
public interface UserFacade {

    UserDTO inc(UserDTO userDTO);
}
