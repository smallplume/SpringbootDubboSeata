package com.applet.service;

import com.applet.dto.CompanyDTO;

/**
 * @author:359428217@qq.com
 * @date:2020/2/2 , 0:03
 * @version:1.0
 **/
public interface CompanyFacade {

    void inc(CompanyDTO companyDTO);
}
