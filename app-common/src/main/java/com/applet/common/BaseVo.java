package com.applet.common;

import com.applet.exceptions.BusinessException;
import com.applet.exceptions.UnknownException;

/**
 * @autor xiaoyu.fang
 * @date 2019/5/28 16:44
 */
public class BaseVo<T> {

    private int code;
    private String msg;
    private T data;

    public BaseVo(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public BaseVo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseVo() {
        this.code = ResultCode.SUCCESS.getCode();
        this.msg = ResultCode.SUCCESS.getInfo();
    }

    public static BaseVo error(int code, String msg) {
        return new BaseVo(code, msg);
    }

    public static BaseVo error(UnknownException ex) {
        return new BaseVo(ex.getCode(), ex.getMsg());
    }

    public static BaseVo error(BusinessException ex) {
        return new BaseVo(ex.getCode(), ex.getMessage());
    }

    public BaseVo(T data) {
        this.data = data;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
