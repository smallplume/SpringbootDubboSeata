package com.applet.common;

/**
 * @autor xiaoyu.fang
 * @date 2019/5/28 16:46
 */
public enum ResultCode {

    SUCCESS(0, "成功"),
    WRONG(1, "操作失败"),
    SERVICE_ERROR(100, "服务暂不可用"),
    FAST(101, "访问过快"),
    UNAUTH(401, "未授权"),
    PARAMETER_ERROR(400, "参数错误"),
    HYSTRIX_TIME_OUT(-1, "连接超时"),
    ERR_SYS_PARAM(1000, "系统异常"),
    ERR_CHECK_PARAM(1010, "参数检验失败"),
    ERR_MISS_PARAM(1011, "参数缺失");

    private int code;
    private String info;

    private ResultCode(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return this.code;
    }

    public String getInfo() {
        return this.info;
    }

}
