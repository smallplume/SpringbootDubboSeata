package com.applet.exceptions;

import com.applet.common.ResultCode;

/**
 * 未知异常
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 11:26
 */
public class UnknownException extends RuntimeException {
    private int code;
    private String msg;
    private String subMsg;

    public UnknownException() {
        this((String)null, (Throwable)null);
    }

    public UnknownException(String subMsg) {
        this(subMsg, (Throwable)null);
    }

    public UnknownException(String subMsg, Throwable cause) {
        this(ResultCode.SERVICE_ERROR.getCode(), ResultCode.SERVICE_ERROR.getInfo(), subMsg, cause);
    }

    protected UnknownException(int code, String msg, String subMsg, Throwable cause) {
        super(combineMessage(msg, subMsg));
        if (cause != null) {
            this.initCause(cause);
        }
        this.code = code;
        this.msg = msg;
        this.subMsg = subMsg;
    }

    private static String combineMessage(String msg, String subMsg) {
        return msg + ", detail: " + subMsg;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSubMsg() {
        return this.subMsg;
    }

    public void setSubMsg(String subMsg) {
        this.subMsg = subMsg;
    }

}
