package com.applet.exceptions;

/**
 * 业务异常
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 11:19
 */
public class BusinessException extends RuntimeException {

    private int code;

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, int code) {
        super(message);
        this.code = code;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
