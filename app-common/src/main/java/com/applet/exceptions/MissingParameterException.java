package com.applet.exceptions;

import com.applet.common.ResultCode;

/**
 * 缺少参数异常
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 11:32
 */
public class MissingParameterException extends UnknownException {

    public MissingParameterException() {
        this((String)null);
    }

    public MissingParameterException(String message) {
        this(message, (Throwable)null);
    }

    public MissingParameterException(String subMsg, Throwable cause) {
        super(ResultCode.PARAMETER_ERROR.getCode(), ResultCode.PARAMETER_ERROR.getInfo(), subMsg, cause);
    }

}
